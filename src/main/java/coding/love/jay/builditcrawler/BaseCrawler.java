package coding.love.jay.builditcrawler;

import static java.util.stream.Collectors.toList;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.http.HttpStatus;

public class BaseCrawler extends WebCrawler {

	private static List<String> DOMAINS_TO_TRACK = new ArrayList<>();

	public static void configure(List<String> links2Follow) {
		BaseCrawler.DOMAINS_TO_TRACK = links2Follow;
	}

	/**
	 * set the domains needed to be tracked.
	 */
	@Override
	public boolean shouldVisit(Page referringPage, WebURL url) {
		return DOMAINS_TO_TRACK.stream()
			.anyMatch(link -> url.getDomain().toLowerCase().contains(link.toLowerCase()));
	}

	/**
	 * Called when a page is fetched and ready to be processed
	 */
	@Override
	public void visit(Page page) {

		if (page.getParseData() instanceof HtmlParseData) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
			Set<WebURL> links = htmlParseData.getOutgoingUrls();

			logger.info("The url to track: {}",  page.getWebURL().getURL());

			logger.debug("Number of outgoing links: {}", links.size());

			logger.info("The outgoing links are : {}", String.join("," ,links.stream().map(WebURL::getURL).collect(toList())));

		}

		logger.info("=============");
	}

	/**
	 * handle the pages with no response
	 */
	@Override
	protected void handlePageStatusCode(WebURL webUrl, int statusCode, String statusDescription) {

		if (statusCode != HttpStatus.SC_OK) {

			if (statusCode == HttpStatus.SC_NOT_FOUND) {
				logger.warn("Broken link: {}, this link was found in page: {}", webUrl.getURL(),
					webUrl.getParentUrl());
			} else {
				logger.warn("Non success status for link: {} status code: {}, description: ",
					webUrl.getURL(), statusCode, statusDescription);
			}
		}
	}
}

package coding.love.jay.builditcrawler;

import com.google.common.net.InternetDomainName;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import edu.uci.ics.crawler4j.url.WebURL;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CrawlerServiceImpl implements CrawlerService {

	@Value("${numberOfCrawlers}")
	private int numberOfCrawlers = 1;

	@Value("${tmpFolder}")
	private String rootFolder;

	@Override
	public void crawler(String url) throws Exception {
		CrawlConfig config = new CrawlConfig();
		config.setCrawlStorageFolder(rootFolder);

		/*
		 * Since images are binary content, we need to set this parameter to
		 * true to make sure they are included in the crawl.
		 */
		config.setIncludeBinaryContentInCrawling(true);

		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
		CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

		controller.addSeed(url);

		final String host = new URL(url).getHost();
		final String domain = InternetDomainName.from(host).topPrivateDomain().toString();
		BaseCrawler.configure(Arrays.asList(domain));

		controller.start(BaseCrawler.class, numberOfCrawlers);
	}
}

package coding.love.jay.builditcrawler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuilditCrawlerApplication implements CommandLineRunner {

	@Autowired
	private CrawlerService crawlerService;

	public static void main(String[] args) {
		SpringApplication.run(BuilditCrawlerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		crawlerService.crawler("https://buildit.wiprodigital.com");
	}
}
# Buildit-crawler

This is a sample crawler for crawling all the links of a website.


## How to build and run your solution

This is a spring boot application. You could clone the codes and run them directly as follows:

```bash
mvn spring-boot:run
```

Currently, all the links are printed in the logs. You could find it under `target/test-logs/crawler4j.log`. It consists:

- `The url to track`: which shows the parent link. The parent link should be under main domain;
- `The outgoing links are`: which shows the links found in the parent link. They would have domain links and other outgoing links such as twitter.
- Some other links such as `Non success status for link` or `Broken link`: shows the parent link is not accessible.

# Reasoning and describe any trade offs

My plan of solving this problem:

1. Firstly, I would like to see if the community has tackled this problem or not. 
I would read their code or documentation to see if their work could solve the problem I have, because I don't want to invent the wheel again. 
I could improve their work or make the customisation as to my problem.

2. As to the `crawl4j`, it matches the problem I have, it provides the very basic codes to crawl a website;


# Explanation of what could be done with more time

Due to the time limit, I have just read the document and implemented the very basic version of crawler a website, which is using `crawl4j`.

My next stages of improving the code:

- support file or database to store all the links;
- add test cases. Currently, all the links are shown in the log. No test cases are needed;
- make the code running async;
- customise the code to use queue to store the intermediate links, so that multiple threads could run concurrently to crawl;
- support multiple environment via profile;
- add CI/CD pipeline to test feature branch and develop branch.